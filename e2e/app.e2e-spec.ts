import { MathNgCliPage } from './app.po';

describe('math-ng-cli App', () => {
  let page: MathNgCliPage;

  beforeEach(() => {
    page = new MathNgCliPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
