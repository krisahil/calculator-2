/**
 * Exports an object that will be accessed globally.
 *
 * Holds miscellaneous settings. A better way is probably to use dependency
 * injection, but this is okay for now.
 */
export var CalcGlobals = {
  isConfigured: false,
};

export function isConfigured() {
  if (!CalcGlobals.isConfigured) {
    alert('Please configure first.');
    return false;
  }
  return true;
}