import { Component, OnInit } from '@angular/core';

import { Problem } from './problem';
import { SELECTIONS } from './selections';
import { PROBLEMS_CORRECT } from './problems-correct';
import { PROBLEMS_INCORRECT } from './problems-incorrect';

@Component({
  moduleId: module.id,
  selector: 'problem-form',
  templateUrl: 'problem-form.component.html',
  styleUrls: ['css/problem-form.component.css'],
  host: {
    '(document:keyup)': 'onDocumentKeyUp($event)',
  }
})

export class ProblemFormComponent implements OnInit {
  static readyForNewProblem:boolean = true;
  static problem: Problem;
  selections = SELECTIONS;
  problemsCorrect = PROBLEMS_CORRECT;
  problemsIncorrect = PROBLEMS_INCORRECT;

  response: string;
  isError: boolean = false;

  /**
   * Listens for number and enter key presses, like a real calculator.
   */
  onDocumentKeyUp(ev: KeyboardEvent) {
    if (/^[0-9]$/.test(ev.key)) {
      this.pushButton(ev.key);
    }
    else if (ev.key === 'Backspace') {
      this.clearResponse();
    }
    else if (ev.key === 'Enter') {
      this.submitResponse();
    }
  }

  ngOnInit(): void {
    this.startProblem();
  }

  startProblem(): void {
    this.isError = false;
    this.response = '?';
    if (ProblemFormComponent.readyForNewProblem) {
      ProblemFormComponent.problem = this.buildProblem();
      ProblemFormComponent.readyForNewProblem = false;
    }
  }

  pushButton(value: string): void {
    // We want the response to be a string, but need to strip the leading
    // question mark, in case it's still there.
    let responseRaw = this.response + value;
    this.response = responseRaw.replace(/^\?+/, '');
  }

  // Resets the response.
  clearResponse(): void {
    this.response = '?';
  }

  submitResponse(): void {
    let problem = ProblemFormComponent.problem;
    var delay:number = 8;
    problem.response = parseInt(this.response, 10);
    // Until user enters an answer, the parsed response will be not-a-number.
    if (isNaN(problem.response)) {
      alert('Please enter an answer.');
      return;
    }

    if (problem.answer === problem.response) {
      this.isError = false;
      this.problemsCorrect.push(problem);
    }
    else {
      this.isError = true;
      // Show the error color for a long time if user erred,
      // so that an error isn't missed.
      delay = 700;
      this.problemsIncorrect.push(problem);
    }

    ProblemFormComponent.readyForNewProblem = true;
    setTimeout(() => {this.startProblem();}, delay);
  }

  // Gets random operator from enabled list of operators.
  getSelectedOperator(): string {
    let operatorsEnabled = Object.keys(this.selections)
      .filter(key => this.selections[key].enabled);

    return operatorsEnabled[ Math.floor(Math.random() * operatorsEnabled.length) ];
  }

  buildProblem(): Problem {
    // Find a random selection from the enabled ones.
    let operator = this.getSelectedOperator();
    let selection = this.selections[operator];

    if (selection) {
      let top:number;
      let bottom:number;
      let answer:number;

      // Determines top, bottom, and answer, depending on operator.
      switch (operator) {
        // Division problems are the complex ones. The answer is actually the
        // top operand.
        case '/':
          answer =  this.getOperand(selection.ranges.top.start, selection.ranges.top.end);
          bottom = this.getOperand(selection.ranges.bottom.start, selection.ranges.bottom.end);
          // Validation ensures that bottom.star and bottom.end cannot both be
          // zero, so this shouldn't cause an infinite loop.
          while (bottom === 0) {
            bottom = this.getOperand(selection.ranges.bottom.start, selection.ranges.bottom.end);
          }
          top = bottom * answer;
          break;

        case '-':
          top = this.getOperand(selection.ranges.top.start, selection.ranges.top.end);
          bottom = this.getOperand(selection.ranges.bottom.start, selection.ranges.bottom.end);
          // Prevents negative answers.
          if (bottom > top) {
            let origTop = top;
            let origBottom = bottom;
            top = origBottom
            bottom = origTop;
          }
          answer = this.calculateAnswer(operator, top, bottom);
          break;

        case '+':
        default:
          top = this.getOperand(selection.ranges.top.start, selection.ranges.top.end);
          bottom = this.getOperand(selection.ranges.bottom.start, selection.ranges.bottom.end);
          answer = this.calculateAnswer(operator, top, bottom);
          break;
      }

      return new Problem(
        operator,
        top,
        bottom,
        answer,
        0
      );
    }

    return null;
  }

  getProblem(): Problem {
    return ProblemFormComponent.problem;
  }

  // Logic stolen from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random.
  getOperand(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1)) + min
  }

  calculateAnswer(operator: string, operand1: number, operand2: number): number {
    let answer: number;
    switch (operator) {
      case '+':
        answer = operand1 + operand2;
        break;

      case '-':
        if (operand1 < operand2) {
          operand1 = operand2;
          operand2 = operand1;
        }
        answer = operand1 - operand2;
        break;

      case 'x':
        answer = operand1 * operand2;
        break;

      case '/':
        answer = operand1 / operand2;
        break;
    }
    return answer;
  }
}