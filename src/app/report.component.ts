import { Component, OnInit } from '@angular/core';

import { Problem } from './problem';
import { PROBLEMS_CORRECT } from './problems-correct';
import { PROBLEMS_INCORRECT } from './problems-incorrect';

@Component({
  moduleId: module.id,
  selector: 'report',
  templateUrl: 'report.component.html',
})

export class ReportComponent implements OnInit {
  correct = PROBLEMS_CORRECT;
  incorrect = PROBLEMS_INCORRECT;
  // Count of both correct and incorrect problems.
  total = this.correct.length + this.incorrect.length;
  showAll:boolean = false;
  // The full list of problems isn't shown until this number of seconds has
  // passed. This is to annoy users who look at previous problems for answers.
  secondsRemaining:number = 30;

  ngOnInit(): void {
    var self = this;
    var id = setInterval(function() {
      --self.secondsRemaining;
      if (self.secondsRemaining <= 0) {
        self.showAll = true;
        clearInterval(id);
      }
    }, 1000);
  }

  getProblems(type:string): Problem[] {
    let problems: Problem[] = [];

    if (type === 'correct') {
      // Shows only the latest five items, because users shouldn't look at
      // previous correct problems for answers.
      problems = this.correct.slice(-5).reverse();
    }
    if (type === 'incorrect') {
      problems = this.incorrect;
    }
    if (type === 'all') {
      problems = this.correct.concat(this.incorrect);
    }

    return problems;
  }
}