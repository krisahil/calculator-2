import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SetupComponent } from './setup.component';
import { ReportComponent } from './report.component';
import { ProblemFormComponent } from './problem-form.component';

const routes: Routes = [
  { path: '', redirectTo: '/setup', pathMatch: 'full' },
  { path: 'setup',  component: SetupComponent },
  { path: 'report',  component: ReportComponent },
  {
    path: 'play',
    component: ProblemFormComponent,
    canActivate: ['CalculatorIsConfigured'],
  },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
