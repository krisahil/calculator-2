import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AppComponent }  from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SetupComponent } from './setup.component';
import { ProblemFormComponent } from './problem-form.component';
import { ReportComponent } from './report.component';
import { CalcGlobals, isConfigured } from './calc-globals';


@NgModule({
  imports: [
    BrowserModule,
    ReactiveFormsModule ,
    AppRoutingModule,
  ],
  providers: [
    {
      provide: 'CalculatorIsConfigured',
      useValue: isConfigured,
    },
  ],
  declarations: [
    AppComponent,
    SetupComponent,
    ProblemFormComponent,
    ReportComponent,
  ],
  bootstrap: [
    AppComponent,
  ],
})
export class AppModule { }
