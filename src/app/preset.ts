import { OperandDetails } from './operand-details';

export interface Preset {
  [name: string]: OperandDetails;
}