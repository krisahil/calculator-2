import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  moduleId: module.id,
  template: `
    <!--<h1>{{title}}</h1>-->
    <nav class="navbar navbar-default">
      <div class="container">
        <ul class="nav navbar-nav">
          <li [routerLinkActive]="['active']"><a routerLink="/setup" routerLinkActive="active">Configure</a></li>
          <li [routerLinkActive]="['active']"><a routerLink="/report" routerLinkActive="active">Report</a></li>
          <li [routerLinkActive]="['active']"><a routerLink="/play" routerLinkActive="active">Play</a></li>
        </ul>
      </div>
    </nav>
    <router-outlet></router-outlet>
  `,
  styleUrls: ['css/app.component.css'],
})

export class AppComponent {
  title = 'Flash cards';
}
