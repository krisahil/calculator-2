export class Option {
  // Name is optional so that each preset doesn't need to define it.
  name?: string;
  enabled: boolean;
  ranges: {
    top: { start: number, end: number},
    bottom: { start: number, end: number},
  }
}