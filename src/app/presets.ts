import { Preset } from './preset';

/**
 * Defines presets for operators, their statuses, and their ranges of operands.
 *
 * TODO Figure out how to define this without code.
 */
export var PRESETS: Preset = {
  'Chaim': {
    '+': {
      enabled: true,
      ranges: {
        top: { start: 2, end: 12},
        bottom: { start: 2, end: 12},
      },
    },
    '-': {
      enabled: true,
      ranges: {
        top: { start: 2, end: 24},
        bottom: { start: 2, end: 12},
      },
    },
    'x': {
      enabled: true,
      ranges: {
        top: { start: 2, end: 10},
        bottom: { start: 2, end: 10},
      },
    },
    '/': {
      enabled: true,
      ranges: {
        top: { start: 2, end: 10},
        bottom: { start: 2, end: 10},
      },
    },
  },
  'Elias': {
    '+': {
      enabled: true,
      ranges: {
        top: { start: 2, end: 12},
        bottom: { start: 2, end: 12},
      },
    },
    '-': {
      enabled: true,
      ranges: {
        top: { start: 2, end: 24},
        bottom: { start: 2, end: 12},
      },
    },
    'x': {
      enabled: true,
      ranges: {
        top: { start: 3, end: 11},
        bottom: { start: 3, end: 11},
      },
    },
    '/': {
      enabled: true,
      ranges: {
        top: { start: 2, end: 9},
        bottom: { start: 2, end: 9},
      },
    },
  },
  'Raia': {
    '+': {
      enabled: true,
      ranges: {
        top: { start: 1, end: 9},
        bottom: { start: 1, end: 9},
      },
    },
    '-': {
      enabled: true,
      ranges: {
        top: { start: 2, end: 18},
        bottom: { start: 2, end: 9},
      },
    },
    'x': {
      enabled: true,
      ranges: {
        top: { start: 2, end: 8},
        bottom: { start: 2, end: 8},
      },
    },
    '/': {
      enabled: false,
      ranges: {
        top: { start: 1, end: 5},
        bottom: { start: 1, end: 5},
      },
    },
  },
}
