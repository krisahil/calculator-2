export class Problem {
  constructor(
    public operator: string,
    public operand1: number,
    public operand2: number,
    public answer: number,
    public response: number
  ) {}
}