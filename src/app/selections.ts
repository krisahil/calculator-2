import { Option } from './option';
import { OperandDetails } from './operand-details';
import { Preset } from './preset';

/**
 * Defines the four core math operators and details of their use.
 *
 * In case there are more operators later (I'm not a math expert), our app can
 * handle them!
 *
 * On a non-math note, I should probably use new ES6 Map, but this "dictionary"
 * works fine http://stackoverflow.com/a/13631733.
 */
export var SELECTIONS: OperandDetails = {
  '+': {
    name: 'Addition',
    enabled: false,
    ranges: {
      top: { start: 0, end: 0},
      bottom: { start: 0, end: 0},
    },
  },
  '-': {
    name: 'Substraction',
    enabled: false,
    ranges: {
      top: { start: 0, end: 0},
      bottom: { start: 0, end: 0},
    },
  },
  'x': {
    name: 'Multiplication',
    enabled: false,
    ranges: {
      top: { start: 0, end: 0},
      bottom: { start: 0, end: 0},
    },
  },
  '/': {
    name: 'Division',
    enabled: false,
    ranges: {
      top: { start: 0, end: 0},
      bottom: { start: 0, end: 0},
    },
  },
};
