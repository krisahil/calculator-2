import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { Problem } from './problem';
import { SELECTIONS } from './selections';
import { PRESETS } from './presets';
import { CalcGlobals } from './calc-globals';

@Component({
  moduleId: module.id,
  selector: 'setup',
  templateUrl: 'setup.component.html',
  styleUrls: ['css/setup.component.css'],
})

export class SetupComponent implements OnInit {
  form: FormGroup;
  selections = SELECTIONS;
  presets = PRESETS;
  presetNames:string[] = Object.keys(this.presets);
  operators:string[] = Object.keys(this.selections);

  defaultError:string = 'Select at least one type.';
  error:string = this.defaultError;
  currentPreset:string = '_none';
  emptyPresetValue:string = this.currentPreset;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    var groups = {};

    // For each operator, build form elements to collect whether that operator
    // is enabled, and, if enabled, the range of its operands.
    for (var operator in this.selections) {
      var option = this.selections[operator];

      groups[ operator ] = new FormGroup({
        enabled: new FormControl(option.enabled),
        ranges: new FormGroup({
          top: new FormGroup({
            start: new FormControl(option.ranges.top.start),
            end: new FormControl(option.ranges.top.end),
          }),
          bottom: new FormGroup({
            start: new FormControl(option.ranges.bottom.start),
            end: new FormControl(option.ranges.bottom.end),
          }),
        }),
      });
    }

    groups['preset'] = new FormControl(this.currentPreset);
    this.form = this.fb.group(groups);

    // When the form values are updated, this validates data. If no validation
    // errors, the data is "submitted", by being written to this.selections
    // object.
    this.form.valueChanges.subscribe((values) => {

      // If new preset selected, injects the preset's values as this form's
      // values.
      if (values.preset && values.preset !== "_none" && values.preset !== this.currentPreset) {
        this.currentPreset = values.preset;
        let preset = this.presets[ this.currentPreset ];
        this.operators.forEach(function(operator) {
          values[operator] = preset[operator];
        });

        (<FormGroup>this.form).setValue(values, { onlySelf: true});
      }

      let operatorValues = {};
      this.operators.forEach(function(operator) {
        operatorValues[operator] = values[operator];
      });

      this.error = '';
      var passes:boolean = false;

      // If any operators enabled, validation passes (so far).
      for (var sign in operatorValues) {
        if (operatorValues[sign].enabled) {
          passes = true;
          break;
        }
      }

      if (!passes) {
        this.error = this.defaultError;
      }

      // But, validation fails if any operand ranges have negative numbers.
      // Someday the app may support negatives.
      for (var sign in operatorValues) {
        if (
          operatorValues[sign].enabled
          && (
            operatorValues[sign].ranges.top.start < 0
            || operatorValues[sign].ranges.top.end < 0
            || operatorValues[sign].ranges.bottom.start < 0
            || operatorValues[sign].ranges.bottom.end < 0
          )) {
          passes = false;
          this.error = 'Do not use any negative numbers';
          break;
        }
      }

      // In an operands range, the first number cannot be higher than second.
      for (var sign in operatorValues) {
        let ranges = operatorValues[sign]['ranges'];
        if ((ranges.top.start > ranges.top.end) || (ranges.bottom.start > ranges.bottom.end)) {
          passes = false;
          this.error = 'The first number cannot be greater than second number.';
          break;
        }
      }

      // Prevents divide-by-zero.
      if (passes
          && operatorValues['/'].enabled
          && operatorValues['/'].ranges.bottom.start === 0
          && operatorValues['/'].ranges.bottom.end === 0) {
        this.error = 'For division, both bottom numbers cannot be zero.';
        passes = false;
      }

      CalcGlobals.isConfigured = passes;
      // Finally, "submit" the data, by writing it to object this.selections.
      // That object will be read by the problem-form controller.
      for (var sign in operatorValues) {
        let option = operatorValues[sign];

        this.selections[sign].enabled = option.enabled;
        this.selections[sign].ranges.top.start = option.ranges.top.start;
        this.selections[sign].ranges.top.end = option.ranges.top.end;
        this.selections[sign].ranges.bottom.start = option.ranges.bottom.start;
        this.selections[sign].ranges.bottom.end = option.ranges.bottom.end;
      }
    });
  }

  /**
   * Resets the form values.
   */
  reset(): void {
    this.currentPreset = this.emptyPresetValue;
    this.form.reset({preset: this.emptyPresetValue});
  }
}
