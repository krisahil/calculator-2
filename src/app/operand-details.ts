import { Option } from './option';

export interface OperandDetails {
  [operator: string]: Option;
}